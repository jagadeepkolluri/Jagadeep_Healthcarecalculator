
public class Calculate {

	public static void main(String args[]){
		double basePremium = 5000;
		int habitsCount = 0 ;
		Person person = new Person();
		person.setFirstName("Norman");
		person.setLastName("Gomes");
		person.setAge(34);
		person.setGender("Male");
		person.setHyperTension(false);
		person.setBloodPressure(false);
		person.setBloodSugar(false);
		person.setOverWeight(true);
		person.setSmoking(false);
		person.setAlcohol(true);
		person.setDailyExcercise(true);
		person.setDrugs(false);

		if(person.getAge()>=18){
			basePremium = addPercentage(basePremium, 10);
		}
		if(person.getAge()>=25){
			basePremium = addPercentage(basePremium, 10);
		}
		if(person.getAge()>=30){
			basePremium = addPercentage(basePremium, 10);
		}
		if(person.getAge()>=35){
			basePremium = addPercentage(basePremium, 10);
		}
		if(person.getAge()>=40){
			int age = person.getAge()-40 ;

			basePremium = addPercentage(basePremium, 20);

			while(age>=5){
				basePremium = addPercentage(basePremium, 20);
				age -= 5;

			}


		}

		if(person.getGender().toLowerCase().equals("male")){
			basePremium = addPercentage(basePremium, 2);
		}

		if(person.isHyperTension()){
			basePremium = addPercentage(basePremium, 1);
		}
		if(person.isBloodPressure()){
			basePremium = addPercentage(basePremium, 1);
		}
		if(person.isBloodSugar()){
			basePremium = addPercentage(basePremium, 1);
		}
		if(person.isOverWeight()){
			basePremium = addPercentage(basePremium, 1);
		}

		if(person.isSmoking()){
			habitsCount++;
		}
		if(person.isAlcohol()){
			habitsCount++;
		}
		if(person.isDrugs()){
			habitsCount++;
		}
		if(person.isDailyExcercise()){
			habitsCount--;
		}
		if(habitsCount<0){
			basePremium = subPercentage(basePremium, 3);
		}else{
			while(habitsCount>0){
				basePremium = addPercentage(basePremium, 3);
				habitsCount--;
			}
		}
		System.out.println("Health Insurance Premium for Mr."+person.getLastName()+" RS:"+Math.round(basePremium));

	}


	public static double addPercentage(double base,int percentage){
		double res = base+((base*percentage)/100);
		return res;
	}

	public static double subPercentage(double base,int percentage){
		double res = base-((base*percentage)/100);
		return res;
	}
}
